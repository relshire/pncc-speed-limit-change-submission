# PNCC Speed Limit Change Submission

This repository contains code, data files, and the pdf of the submission that I wrote for the Palmerston North City Council's proposal to change the speed limits in our area. I put it here in the hope that PNCC uses it to generate maps that can inform future proposal writing.

The output of this script was published here: https://rpubs.com/r_elshire/1104141 in October of 2023. I have no idea how long it will stay there.

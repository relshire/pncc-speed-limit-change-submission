library(leaflet)
library(dplyr)  # If you need to manipulate data frames

turitea_map <- leaflet() %>%
  addTiles()  # Adds the default OpenStreetMap tiles
# Add data frame with lat and long
# Adjust the location of the input file.
turitea_locations <- read.csv("./turitea_locations.csv")
# Add points from your data frame
turitea_map <- turitea_map %>% addMarkers(data = turitea_locations, ~Longitude, ~Latitude)

# Set the initial view to focus on Turitea Valley
turitea_map <- setView(lng = 175.5965, lat = -40.3727, zoom = 13)

# Show the map
turitea_map

